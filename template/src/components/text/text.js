var text = new function () {

  //catch DOM
  var object;
  var slider;

  //bind events
  $(document).ready(function () {
    init();
  });

  //private function
  var init = function () {
    object = $('.text');
    slider = object.find('.text__slider');

    sliderMake();
  };

  var sliderMake = function () {
    if (slider.length > 0) {
      slider.slick({
        infinite: false,
        dots: false,
        arrows: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: false,
        adaptiveHeight: true,
        autoplay: true,
        speed: 1000,
        autoplaySpeed: 1500,
        draggable: false,
        pauseOnHover: false,
        pauseOnFocus: false,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      });
    }
  };
};