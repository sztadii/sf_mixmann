var menu = new function () {

  //catch DOM
  var $body;
  var $button;
  var $background;
  var $listMain;
  var $list;

  //private vars
  var menuBreakpoint = 1024;

  //bind events
  $(document).ready(function () {
    $body = $('body');

    $button = $('.menu__button');
    $background = $('.menu .menu__background');
    $listMain = $('.menu .menu__list.-main');
    $list = $('.menu__list');

    checkSuperfish();
    makeAccordion();
    triggerMenu();
  });

  $(window).on('debouncedresize', function () {
    checkSuperfish();
  });

  //private functions
  var checkSuperfish = function () {
    if ($body.width() >= menuBreakpoint) {
      initSuperfish();

    } else {
      removeSuperFish();
    }
  };

  var initSuperfish = function () {
    $listMain.superfish({
      delay: 3000,
      cssArrows: false,
      onHide: function () {
        //TODO
      }
    });
  };

  var triggerMenu = function () {
    $button.on('click', function () {
      toggleMenu();
    });

    $background.on('click', function () {
      toggleMenu();
    });

    $('.menu__link').on('click', function () {
      toggleMenu();
    });
  };

  var removeSuperFish = function () {
    $list.attr('style', '');
    $listMain.superfish('destroy');
  };

  var hideMenu = function () {
    $background.fadeOut(500);
    $listMain.slideUp(400, function () {
      $button.removeClass('-active');
    });

    resetMenu();
  };

  var showMenu = function () {
    $background.fadeIn(500);
    $listMain.slideDown(400, function () {
      $button.addClass('-active');
    });
  };

  var toggleMenu = function () {
    if ($(window).width() <= menuBreakpoint) {
      if ($button.hasClass('-active')) {
        hideMenu();
      } else {
        showMenu();
      }
    }
  };

  var makeAccordion = function () {
    $list.not('.-main').on('click', function (event) {
      event.stopPropagation();

      if ($(this).hasClass('-active')) {
        $(this).removeClass('-active').children().slideUp(400);
      } else {
        $(this).parent().siblings()
          .find('.menu__list.-active:not(.-main)')
          .removeClass('-active')
          .children().slideUp();
        $(this).addClass('-active').children().slideDown(400);
      }
    });
  };

  var resetMenu = function () {
    if ($list.hasClass('-active')) {
      $list.removeClass('-active')
        .find('.menu__item')
        .attr('style', '');
    }
  };
};