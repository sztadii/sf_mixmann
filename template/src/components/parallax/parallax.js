function Parallax() {
  var objects;
  var body;
  var breakpoint = 1023;

  $(document).ready(function () {
    body = $('body');

    init();
    start();
  });

  $(window).bind("scroll resize", start);

  function init() {
    objects = [].slice.call(document.querySelectorAll(".parallax"))
  }

  function getWindowHeight() {
    var a = document.documentElement.clientHeight, b = window.innerHeight;
    return b > a ? b : a
  }

  function getWindowOffset() {
    if ("undefined" != typeof window.scrollY) return window.scrollY;
    if ("undefined" != typeof pageYOffset) return pageYOffset;
    var a = document.documentElement;
    return a = a.clientHeight ? a : document.body, a.scrollTop
  }

  function start() {
    if (body.width() > breakpoint) {
      var a, d, f, g = getWindowOffset(), h = getWindowHeight();
      for (var i in objects)
        a = objects[i], d = a.offsetTop, f = a.offsetHeight, d > g + h || g > d + f || (a.style.backgroundPosition = "50% " + Math.round(2 * (d - g) / 8) + "px")
    }
  }
}

var parallax = new Parallax();